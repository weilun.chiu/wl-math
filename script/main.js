const exception = ["，", "、", "。", "：", "！", "？"];

$(document).ready(function(){
    $("#btn-start").click(transfer);
    $("#btn-exc").click(insertExc);
});

function transfer(){
    var input = $("#input-text").val();
    $("#output-text").val(latexTransfer(input.replace(/\s/g, '')));
}

function insertExc(){
    var exc = $("#custom-text").val();
    var msg = "Insert: ";
    
    for(var index = 0; index<exc.length; index++){
        exception.push(exc[index]);
        msg = msg + exc[index] + ", ";
    }
    
    alert(msg + "!");
}

function latexTransfer(input){
    var output = "";
    
    console.log(input);
    
    var preType = -1;
    
    // beginning $ for LN at first
    if(isLN(input[0]) == 1){
        output = output + "$";
        preType = 1;
    }
    
    // body
    for(var index = 0; index<input.length; index++){
        curType = isLN(input[index]);
        
        if(preType == -1 && curType == 1){
            output = output + " $";
            output = output + input[index];
        } else if (preType == 1 && curType == -1){
            output = output + "$ ";
            output = output + input[index];
            
        } else if(preType === 0 && curType == 1){
            output = output + "$";
            output = output + input[index];
        } else if (preType == 1 && curType === 0){
            output = output + "$";
            output = output + input[index];
            
        } else {
            output = output + input[index];
        }
        
        preType = curType;
    }
    
    // endding $ for LN at last
    // console.log(preType);
    if(preType == 1) output = output + "$";
    
    return output;
}

function isLN(char){
    var letterNumber = /[\u4e00-\u9fff]|[\u3400-\u4dbf]|[\u{20000}-\u{2a6df}]|[\u{2a700}-\u{2b73f}]|[\u{2b740}-\u{2b81f}]|[\u{2b820}-\u{2ceaf}]|[\uf900-\ufaff]|[\u3300-\u33ff]|[\ufe30-\ufe4f]|[\uf900-\ufaff]|[\u{2f800}-\u{2fa1f}]/u;
    
    if(char.match(letterNumber) === null){
        if(exception.indexOf(char) >= 0){
            // is one of exception
            return 0;
        } else return 1;
    } else {
        // real Chinese letter
        return -1;
    }
}